#!/bin/sh
############################################################################
#
# MODULE:       "g.region.ll" 
#
# AUTHOR:       "Alessandro Frigeri"
#               
#
# PURPOSE:      "set computational region giving lat lon coordinates"
#               
#
# COPYRIGHT:    (c) 2012 by the GRASS Development Team
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file COPYING that comes with GRASS
#               for details.
#
# REQUIRES:     "no specific requirements - uses g.region"
#               
#
#############################################################################

#%Module
#% description: set computational region giving latlong coordinates
#%End
#%option
#% guisection: coordinates
#% key: w
#% type: double
#% description: west
#% required: no
#%end
#%option
#% guisection: coordinates
#% key: e
#% type: double
#% description: east
#% required: no
#%end
#%option
#% guisection: coordinates
#% key: n
#% type: double
#% description: north
#% required: no
#%end
#%option
#% guisection: coordinates
#% key: s
#% type: double
#% description: south
#% required: no
#%end

if  [ -z $GISBASE ] ; then
  echo "You must be in GRASS GIS to run this program."
  exit 1
fi   

if [ "$1" != "@ARGS_PARSED@" ] ; then
  exec g.parser "$0" "$@"
fi

PROG=`basename "$0"`

w="$GIS_OPT_W"
e="$GIS_OPT_E"
n="$GIS_OPT_N"
s="$GIS_OPT_S"

a=`g.proj -j | grep '+a' | cut -d'=' -f2`
b=`g.proj -j | grep '+b' | cut -d'=' -f2`
rf=`g.proj -j | grep '+rf' | cut -d'=' -f2`

if test $a -a $b  
	then
        lldef="\"+proj=latlong +a=$a +b=$b\""
fi

if test $a -a $rf
	then
        lldef="\"+proj=latlong +a=$a +rf=$rf\""
fi

if test $b -a $rf
	then
        lldef="\"+proj=latlong +b=$b +rf=$rf\""
fi

this_proj=\"`g.proj -fj`\"

MPROJ="m.proj -d proj_in=$lldef proj_out=$this_proj --q"

ll=`echo $w $s | eval $MPROJ`
ur=`echo $e $n | eval $MPROJ`

w_m=`echo $ll | cut -f1 -d" "`
s_m=`echo $ll | cut -f2 -d" "`
e_m=`echo $ur | cut -f1 -d" "`
n_m=`echo $ur | cut -f2 -d" "`

g.region w=$w_m e=$e_m s=$s_m n=$n_m 

# Exit - 0 is success
exit 0
